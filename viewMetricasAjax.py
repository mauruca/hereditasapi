import locale
from rest_framework.permissions import *
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from hereditascore.models import Bem, Depreciacao, Inventario
from django.db.models import Count, Max, Min, Q, Sum
from .serMetricasAjax import *

# contagens
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def contagemMetricas(self):
    # Contagem e valor de bens
    contagemBens = Bem.objects.exclude(ativo=False).count()
    if not contagemBens or contagemBens == 0:
        valorBens = 0
    else:
        valorBens = Bem.objects.exclude(ativo=False).aggregate(Sum('valoratual'))['valoratual__sum']
        if not valorBens:
            valorBens = 0

    contagemBensEtiquetados = Bem.objects.exclude(ativo=False).exclude(rfid__isnull=True).exclude(rfid='').count()
    if not contagemBensEtiquetados or contagemBensEtiquetados == 0:
        valorBensEtiquetados = 0
    else:
        valorBensEtiquetados = Bem.objects.exclude(ativo=False).exclude(rfid__isnull=True).exclude(rfid='').aggregate(Sum('valoratual'))['valoratual__sum']
        if not valorBensEtiquetados:
            valorBensEtiquetados = 0

    # Grafico torta de bens etiquetados
    contagemBensNaoEtiquetados = contagemBens - contagemBensEtiquetados
    valorBensNaoEtiquetados = valorBens - valorBensEtiquetados

    contagemDepreciacoes = Depreciacao.objects.count()
    contagemInventarios = Inventario.objects.count()

    contagem = Contagem(contagemBens,valorBens,contagemBensEtiquetados,valorBensEtiquetados,contagemBensNaoEtiquetados,valorBensNaoEtiquetados,contagemDepreciacoes,contagemInventarios)
    serContagem = ContagemSerializer(contagem)

    return Response(serContagem.data)

# Metrica de estado do bem
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def estadoBem(request):
    dados = contagemPorColuna("Estado",Bem.ESTADO_BEM)
    return Response({"bensporEstadoDados":dados})

# Metrica de situacao do bem
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def situacaoBem(request):
    dados = contagemPorColuna("Situacao",Bem.SITUACAO_BEM)
    return Response({"bensporSituacaoDados":dados})


def contagemPorColuna(coluna,dictColuna):
    campo = coluna.lower()
    qs = Bem.objects.exclude(ativo=False).values(campo).annotate(contagem=Count(campo)).order_by(campo)
    dados = ""
    data = ""
    labels = ""
    for item in qs:
        if labels:
          labels = labels + ","
        labels = labels + "'" + dict(dictColuna).get(item[campo]) + "'"
        if data:
          data = data + ","
        data = data + str(item["contagem"])
        if dados:
          dados = dados + ","
        dados = dados + "[" + "'" + dict(dictColuna).get(item[campo]) + "'" + "," + str(item["contagem"]) + "]"
    if not dados:
        dados = "['',0]"
    return "['" + coluna + "', 'Qtd bens']," + dados
