from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

class CustomAuthTokenSerializer(serializers.Serializer):
    email_or_username = serializers.CharField(label=_("email_or_username"))
    username = serializers.CharField(label=_("Username"), required=False)
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})

    def validateEmail(self, email):
        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    def validate(self, attrs):
        email_or_username = attrs.get('email_or_username')
        username = attrs.get('username')
        password = attrs.get('password')
        user = None
        if email_or_username and password:
            # Check if user sent email
            tusername = ""
            if self.validateEmail(email_or_username):
                if User.objects.filter(email=email_or_username).count() == 1:
                    username = User.objects.get(email=email_or_username).username
        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg, code='authorization')
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "email" or "username", and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
