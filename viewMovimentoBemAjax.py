# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import viewsets
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination

# Create your views here.
from .models import *
from .serializers import *
from .pagination import *
from .datatablesApiView import *

def _get_dados(queryset):
    dados = []
    mov = None
    for item in queryset:
        filtro = list(filter(lambda x: x.idBem == item["id"], dados))
        teste = len(filtro) == 0
        if teste: # crio novo objeto
            mov = MovimentosBem()
            mov.adicionaInventario(item)
            dados.append(mov)
        else:
            mov = filtro[0]
            mov.adicionaInventario(item)

    return dados

class MovimentoDatatablesAPIView(DatatablesAPIView):
    def list(self, request, *args, **kwargs):
        search = self._get_param_by_method(request,"search[value]")
        orderby = self._get_param_by_method(request,"order[0][column]")

        queryset = self.get_queryset()

        if search:
            queryset = queryset.filter(self.get_query(search))
        else:
            queryset = self.filter_queryset(queryset)

        if orderby:
            queryset = queryset.order_by(*self.get_orderby(request,orderby))

        dados = _get_dados(queryset)

        page = self.paginate_queryset(dados)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            response = self.get_paginated_response(serializer.data)
        else:
            serializer = self.get_serializer(dados, many=True)
            response = Response(serializer.data)

        return response

class MovimentoBemAjaxViewSet(MovimentoDatatablesAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Bem.objects.exclude(ativo=False).order_by('-inventario__data','id').values('id','descricao','numerotombamento','inventario__data','inventario__local__nome','inventario__user__first_name','inventario__user__last_name')
    serializer_class = MovimentoBemAjaxSerializer
