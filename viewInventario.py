# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import viewsets, mixins

# Create your views here.
from .models import *
from .serializers import *


class InventarioViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Inventario.objects.all()
    serializer_class = InventarioSerializer
