# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *
from .currency import *

class DepreciacaoBemSerializer(serializers.ModelSerializer):

    padraodepreciacao = serializers.StringRelatedField()

    class Meta:
        model = Bem
        fields = ('descricao','padraodepreciacao')

class DepreciacaoAjaxSerializer(serializers.ModelSerializer):
    search_fields = ('id','data','bem__id','bem__descricao','bem__padraodepreciacao__nome','valoranterior','valordepreciado')
    order_fields = ('id','data','bem__descricao','bem__padraodepreciacao__nome','valoranterior','valordepreciado')

    bem = DepreciacaoBemSerializer(many=False, read_only=True)

    valorAnteriorLocalizado = serializers.SerializerMethodField()
    def get_valorAnteriorLocalizado(self,obj):
        return currency(obj.valoranterior)

    valorDepreciadoLocalizado = serializers.SerializerMethodField()
    def get_valorDepreciadoLocalizado(self,obj):
        return currency(obj.valordepreciado)

    class Meta:
        model = Depreciacao
        fields = ('id','data','bem','valorAnteriorLocalizado','valorDepreciadoLocalizado')
        read_only_fields = ('id','data','valoranterior','valordepreciado')
