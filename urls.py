# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
from django.conf.urls import url, include

# rest fw
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers
from rest_framework.authtoken import views
from . import viewToken

# restapi
from .views import *

# interfaces usadas pelo android
router = routers.DefaultRouter()
router.register(r'api/local', LocalViewSet)
router.register(r'api/inventario', InventarioViewSet)
router.register(r'api/bem', BemViewSet)
router.register(r'api/pessoa', PessoaViewSet)

# patterns
urlpatterns = [
    url(r'^', include(router.urls)),
    #url(r'^apiX/auth/', include('rest_framework.urls', namespace='rest_framework')), # comentado pois não usado pelo android
    url(r'^api-token-auth/', views.obtain_auth_token), # padrao do rest framework usado no android
    url(r'^apiX/api-token-auth/', viewToken.get_auth_token_custom),
    # AJAX API path
    url(r'^apiX/bemX/$', BemAjaxViewSet.as_view(), name="bemX"),
    url(r'^apiX/bemAgrupX/$', BemAgrupadoAjaxViewSet.as_view(), name="bemagrupX"),
    url(r'^apiX/inventarioX/$', InventarioAjaxViewSet.as_view(), name="inventarioX"),
    url(r'^apiX/depreciacaoX/$', DepreciacaoAjaxViewSet.as_view(), name="depreciacaoX"),
    url(r'^apiX/movimentoX/$', MovimentoBemAjaxViewSet.as_view(), name="movimentoX"),
    url(r'^apiX/mapaX/$', MapaBemAjaxViewSet.as_view(), name="mapaX"),
    url(r'^apiX/metricasX/contagem$', contagemMetricas, name="contagemX"),
    url(r'^apiX/metricasX/estadoBem$', estadoBem, name="estadoBemX"),
    url(r'^apiX/metricasX/situacaoBem$', situacaoBem, name="situacaoBemX"),
]
#urlpatterns = format_suffix_patterns(urlpatterns)
