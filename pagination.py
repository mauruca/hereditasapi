# Copyright (C) 2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework.pagination import *
from rest_framework.response import Response

def _positive_int(integer_string, strict=False, cutoff=None):
    """
    Cast a string to a strictly positive integer.
    """
    ret = int(integer_string)
    if ret < 0 or (ret == 0 and strict):
        raise ValueError()
    if cutoff:
        ret = min(ret, cutoff)
    return ret

def _get_count(queryset):
    """
    Determine an object count, supporting either querysets or regular lists.
    """
    try:
        return queryset.count()
    except (AttributeError, TypeError):
        return len(queryset)

def _get_param_by_method(request,key):
    if request.method == 'GET':
        return request.query_params[key]
    elif request.method == 'POST':
        return request.data[key]

class APIResultsSetPagination(PageNumberPagination):
    page_size = 25
    page_size_query_param = 'page_size'
    max_page_size = 100

class DatatablesResultsSetPagination(LimitOffsetPagination):
    default_limit = 25
    limit_query_param = 'length'
    offset_query_param = 'start'
    draw_query_param = 'draw'
    max_limit = 1000
    draw = 0

    def paginate_queryset(self, queryset, request, view=None):
        self.draw = self.get_draw(request)
        self.limit = self.get_limit(request)
        if self.limit is None:
            return None

        self.offset = self.get_offset(request)
        self.count = _get_count(queryset)
        self.request = request
        if self.count > self.limit and self.template is not None:
            self.display_page_controls = True
        return list(queryset[self.offset:self.offset + self.limit])

    def get_paginated_response(self, data):
        return Response({
             'draw': self.draw
            ,'recordsTotal': self.count
            ,'recordsFiltered': self.count
            ,'results': data
        })

    def get_draw(self, request):
        try:
            return _positive_int(_get_param_by_method(request,self.draw_query_param))
        except (KeyError, ValueError):
            return 0

    def get_limit(self, request):
        if self.limit_query_param:
            try:
                return _positive_int(_get_param_by_method(request,self.limit_query_param),
                    strict=True,
                    cutoff=self.max_limit
                )
            except (KeyError, ValueError):
                pass

        return self.default_limit

    def get_offset(self, request):
        try:
            return _positive_int(_get_param_by_method(request,self.offset_query_param))
        except (KeyError, ValueError):
            return 0
