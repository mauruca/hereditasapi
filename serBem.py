# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *

class BemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bem
        fields = ('pk','rfid','descricao','numerotombamento')
        read_only_fields = ('pk','descricao','numerotombamento')
